(function () {
    'use strict';

    angular.module('ticket-module')
        .factory('ticketService', ticketService);

    ticketService.$inject = ['$http', '$rootScope'];

    function ticketService($http, $rootScope) {
        return {
            buyTicket: buyTicket,
            createTicketToSave: createTicketToSave
        };

        function buyTicket(ticket) {
            return $http.post('http://localhost:8080/tickets/buy', ticket);
        }

        function createTicketToSave(ticketInfo) {
            var ticketToSave = {};

            ticketToSave.userId = $rootScope.session.userInfo.id;
            ticketToSave.route = {
                name: ticketInfo.route
            };
            ticketToSave.price = ticketInfo.price;

            return ticketToSave;
        }
    }
    }());