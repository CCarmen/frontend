(function () {
    'use strict';

    angular.module('login-module').controller('loginController', loginController);

    loginController.$inject = ['$rootScope', '$scope', '$state', 'loginService','flashService'];

    function loginController($rootScope, $scope, $state, loginService, flashService) {

        var vm = this;

        vm.submitForm = submitForm;
        vm.closeLoginForm = closeLoginForm;
        vm.registerNewUser = registerNewUser;
        vm.logoutUser = logoutUser;

        initLoginController();

        function initLoginController() {
            console.log('initLoginController');

            $rootScope.authenticated = false;

            angular.element(document).find('body').addClass('stop-background-scroll');

            vm.user = {
                isNewUser: false
            };
        }

        function submitForm() {
            console.log('vm.user == ' + JSON.stringify(vm.user));
            var userToLoginService;

            if (vm.user.username) {
                if (vm.user.isNewUser) {
                    var valid = checkConfirmPassword(vm.user.password, vm.user.confirmPassword);

                    if(valid && valid != 'false') {
                        userToLoginService = loginService.createUserForSave(vm.user);
                        loginService.signUp(userToLoginService).then(
                            function (response) {
                                //success
                                onSuccessLogin(response);
                            }, function (error) {
                                //error
                                console.log('error login response ==' + JSON.stringify(error));
                                if (error.status == '404') {
                                    $rootScope.authenticated = false;
                                }
                            });
                    } else {
                        flashService.Error("The confirmation password is different!")
                    }

                } else {
                    userToLoginService = loginService.createLoginUser(vm.user);

                    loginService.loginUser(userToLoginService).then(
                        function (response) {
                            //success
                            onSuccessLogin(response);
                        }, function (error) {
                            //error
                            $rootScope.authenticated = false;
                            console.log('error login response ==' + JSON.stringify(error));
                            flashService.Error("Invalid username or password!");
                        });
                }
            }


        }

        function checkConfirmPassword(password, confirmPassword) {
            if (angular.equals(password, confirmPassword)) {
                vm.loginForm.$invalid = false;
                return true;

            } else {
                vm.loginForm.$invalid = true;
                return false;
            }
        }

        function onSuccessLogin(response) {
            if (response.status == '200' || response.status == '201') {
                console.log('success');
                $rootScope.authenticated = true;
                if (vm.user.isNewUser) {
                    $rootScope.session = {
                        userInfo: response.data
                    }
                } else {
                    $rootScope.session = response.data;
                }
                angular.element(document).find('body').removeClass('stop-background-scroll');
                $state.go('route');

            } else {
                console.log('error = ' + JSON.stringify(response));
                $rootScope.authenticated = false;
                flashService.Error(response.message);
            }
        }

        function closeLoginForm() {
            $state.go('home');
            angular.element(document).find('body').removeClass('stop-background-scroll');
        }

        function registerNewUser() {
            vm.user.isNewUser = true;
        }

        function logoutUser() {
            $state.go('home');
            $rootScope.authenticated = false;
            $rootScope.session = {};
            angular.element(document).find('body').removeClass('stop-background-scroll');
            $state.go('home');
        }
    }

}());