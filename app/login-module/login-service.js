(function () {
    'use strict';

    angular.module('login-module')
        .factory('loginService', loginService);

    loginService.$inject = ['$http', '$rootScope'];

    function loginService($http, $rootScope) {

        return {
            loginUser: loginUser,
            signUp: signUp,
            createUserForSave: createUserForSave,
            createLoginUser: createLoginUser,
        };


        function loginUser(user) {
            console.log('loginUser user == ' + JSON.stringify(user));
            return $http.post('http://localhost:8080/user/login', user); //todo login
        }

        function signUp(user) {
            console.log(' signUp user == ' + JSON.stringify(user));
            return $http.post('http://localhost:8080/user/register', user);
        }

        /**
         * Function used to create user signup object for BE call
         * @param user
         * @returns {{}}
         */
        function createUserForSave(user) {
            var userForSave = {};

            userForSave.firstName = user.firstName;
            userForSave.lastName = user.lastName;
            userForSave.email = user.email;
            userForSave.username = user.username;
            userForSave.password = user.password;
            userForSave.identityCard = user.identityCard;

            return userForSave;
        }

        /**
         * Function used to create user login object for BE call
         * @param user
         * @returns {{}}
         */
        function createLoginUser(user) {
            var loginUser = {};

            loginUser.username = user.username;
            loginUser.password = user.password;

            return loginUser;
        }
    }
}());

