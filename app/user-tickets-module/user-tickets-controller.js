(function () {
    'use strict';

    angular.module('user-tickets-module').controller('userTicketsController', userTicketsController);

    userTicketsController.$inject = ['$rootScope', '$scope','userTicketsService'];

    function userTicketsController($rootScope,$scope,userTicketsService) {
        var vm = this;

        vm.getUserTickets = getUserTickets;

        initUserTicketsController();

        function initUserTicketsController() {
            console.log('initUserTicketsController');
            getUserTickets();
        }

        function getUserTickets() {
            userTicketsService.getUserTickets().then(
                function (response) {
                    //success
                    vm.tickets = response.data;
                }, function (error) {
                    //error
                    console.log('error login response ==' + JSON.stringify(error));
                });
        }
    }
}());