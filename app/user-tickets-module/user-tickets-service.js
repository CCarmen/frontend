(function () {
    'use strict';

    angular.module('user-tickets-module')
        .factory('userTicketsService', userTicketsService);

    userTicketsService.$inject = ['$http', '$rootScope'];

    function userTicketsService($http, $rootScope) {

        return {
            getUserTickets: getUserTickets
        };

        function getUserTickets() {
            return $http.get('http://localhost:8080/tickets/allTickets?id='+ $rootScope.session.userInfo.id);

        }
    }
}());