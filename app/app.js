/**
 * Angular main application module
 */
var app = angular.module('app', ['ui.router',
    'ngResource',
    'login-module',
    'home-module',
    'flash-module',
    'user-profile-module',
    'route-module',
    'ticket-module',
    'user-tickets-module'
]);

app.run(function ($rootScope, $location, $state) {

    $state.go('home');
});

app.config(['$stateProvider', '$urlRouterProvider', '$qProvider',
    function ($stateProvider, $urlRouterProvider, $qProvider) {

        $qProvider.errorOnUnhandledRejections(false);

        var loginState = {
            name: 'login',
            url: '/login',
            templateUrl: '/login-module/login-view.html'
        };

        var homeState = {
            name: 'home',
            url: '/home',
            templateUrl: '/home-module/home-view.html'
        };

        var userProfileState = {
            name: 'userProfile',
            url: '/userProfile',
            templateUrl: '/user-profile-module/user-profile-view.html'
        };

        var routeState = {
            name: 'route',
            url: '/route',
            templateUrl:'/route-module/route-view.html'
        };

        var ticketState = {
            name: 'ticket',
            url: '/ticket',
            templateUrl:'/ticket-module/ticket-view.html'
        };

        var userTicketsState = {
            name: 'userTickets',
            url: '/userTickets',
            templateUrl:'user-tickets-module/user-tickets-view.html'
        };

        $stateProvider.state(loginState);
        $stateProvider.state(homeState);
        $stateProvider.state(userProfileState);
        $stateProvider.state(routeState);
        $stateProvider.state(ticketState);
        $stateProvider.state(userTicketsState);
    }]);
