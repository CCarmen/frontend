(function () {
    'use strict';

    angular.module('route-module').controller('routeController', routeController);

    routeController.$inject = ['$rootScope', '$scope', '$state', 'routeService', 'flashService', 'ticketService'];

    function routeController($rootScope, $scope, $state, routeService, flashService, ticketService) {

        var vm = this;

        vm.route = route;
        vm.buyTicket = buyTicket;
        vm.fillInCoordinatesFrom = fillInCoordinatesFrom;
        vm.fillInCoordinatesTo = fillInCoordinatesTo;

        $rootScope.hasFare = false;

        initRouteController();

        function initRouteController() {
            vm.autocompleteStart = new google.maps.places.Autocomplete(document.getElementById('autocompleteStart'), {
                types: ['geocode']
            });

            google.maps.event.addListener(vm.autocompleteStart, 'place_changed', function() {
                $scope.$evalAsync(fillInCoordinatesFrom);
            });

            vm.autocompleteEnd = new google.maps.places.Autocomplete(document.getElementById('autocompleteEnd'), {
                types: ['geocode']
            });

            google.maps.event.addListener(vm.autocompleteEnd, 'place_changed', function() {
                $scope.$evalAsync(fillInCoordinatesTo);
            });

            vm.directionsService = new google.maps.DirectionsService();
            vm.directionsDisplay = new google.maps.DirectionsRenderer({
                polylineOptions: {
                    strokeColor: "red",
                    strokeWeight: 5
                }
            });
        }

        function fillInCoordinatesFrom() {
            var place = vm.autocompleteStart.getPlace();
            var location = place.geometry.location;
            vm.startLat = location.lat();
            vm.startLng = location.lng();
        }

        function fillInCoordinatesTo() {
            var place = vm.autocompleteEnd.getPlace();
            var location = place.geometry.location;
            vm.endLat = location.lat();
            vm.endLng = location.lng();
        }

        function route() {
            var from = {
                latitude: vm.startLat,
                longitude: vm.startLng
            };
            var to = {
                latitude: vm.endLat,
                longitude: vm.endLng
            };

            var coordinatesToSearch = routeService.createCoordinatesObject(from, to);
            routeService.route(coordinatesToSearch).then(
                function (response) {
                    //success
                    console.log("succes response == " + JSON.stringify(response));
                    checkIfHasFare(response.data);
                    checkPaths(response.data);
                }, function (error) {
                    //error
                    console.log('error login response ==' + JSON.stringify(error));
                });
        }

        function checkIfHasFare(data) {
            vm.tickets = [];
            vm.coordinates = [];
            var paths = data.paths;
            angular.forEach(paths,function(value){
                if (value.fare) {
                    angular.forEach(value.legs,function(valueLegs){
                        angular.forEach(valueLegs.geometry.coordinates, function(coord){
                            vm.coordinates.push(coord);
                        });
                        if (angular.equals(valueLegs.type,'pt')) {
                            vm.hasFare = true;
                            var price = value.fare.substring(2, value.fare.length);
                            vm.tickets.push({'route':valueLegs.trip_headsign,'price':Number(price)});
                        }
                    })
                }
            });
            // showRouteOnMap();
        }

        function checkPaths(data) {
            vm.paths = [];
            angular.forEach(data.paths,function(value){
                vm.paths.push({'time':getTime(value.time),'instructions':value.instructions});
            });
            showRouteOnMap();
        }

        function getTime(time) {
            var tmpTime = Math.round(time / 60 / 1000, 1000);
            var resTimeStr;
            if (tmpTime > 60) {
                if (tmpTime / 60 > 24) {
                    resTimeStr = Math.floor(tmpTime / 60 / 24, 1) + "day";
                    tmpTime = Math.floor(((tmpTime / 60) % 24), 1);
                    if (tmpTime > 0)
                        resTimeStr += " " + tmpTime + "hour";
                } else {
                    resTimeStr = Math.floor(tmpTime / 60, 1) + "hour";
                    tmpTime = Math.floor(tmpTime % 60, 1);
                    if (tmpTime > 0)
                        resTimeStr += " " + tmpTime + "min";
                }
            } else
                resTimeStr = Math.round(tmpTime % 60, 1) + "min";
            return resTimeStr;
        };

        function buyTicket(index) {

            if ($rootScope.session.userInfo.identityCard && $rootScope.session.userInfo.creditCard) {
                var ticketToSave = ticketService.createTicketToSave(vm.tickets[index]);
                ticketService.buyTicket(ticketToSave).then(
                    function (response) {
                        //success
                        console.log("succes response == " + JSON.stringify(response));
                        flashService.Success("You have just bought a ticket!");
                    }, function (error) {
                        //error
                        console.log('error login response ==' + JSON.stringify(error));
                    });
            } else {
                flashService.Error("You must have the identity card and a credit card registered!");
            }
        }

        function showRouteOnMap() {
            vm.mapOptions = {
                zoom:20,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: {lat: vm.coordinates[0][1], lng: vm.coordinates[0][0]}
            };
            vm.map = new google.maps.Map(document.getElementById('map-canvas'), vm.mapOptions);
            vm.directionsDisplay.setMap(vm.map);

            var waypts = [];
            var polylines = [];
            var step = Math.round(vm.coordinates.length/8 + 1);
            var len = vm.coordinates.length - 1;
            for(var i=1; i < len; i+=step) {
                waypts.push({location: vm.coordinates[i][1] + ', ' + vm.coordinates[i][0], stopover: true})
            }
            var request = {
                origin: {
                    lat: vm.coordinates[0][1],
                    lng: vm.coordinates[1][0]
                },
                destination: {
                    lat: vm.coordinates[vm.coordinates.length-1][1],
                    lng: vm.coordinates[vm.coordinates.length-1][0]
                },
                travelMode: google.maps.TravelMode.DRIVING
            };
            console.log(request);
            vm.directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    vm.directionsDisplay.setDirections(response);
                }
            });

        }



    }
}());