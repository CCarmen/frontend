(function () {
    'use strict';

    angular.module('route-module')
        .factory('routeService', routeService);

    routeService.$inject = ['$http', '$rootScope'];

    function routeService($http, $rootScope) {

        return {
            route: route,
            createCoordinatesObject: createCoordinatesObject
        };

        function route(coordinates) {
            console.log(' route for coordinates == ' + JSON.stringify(coordinates));
            return $http.post('http://localhost:8080/route', coordinates);
        }

        function createCoordinatesObject(from,to) {
            var coordinatesObject = {};

            var fromLatString = from.latitude.toString().substring(0,9);
            coordinatesObject.fromLat = Number(fromLatString);
            var fromLonString = from.longitude.toString().substring(0,9);
            coordinatesObject.fromLon = Number(fromLonString);
            var toLatString = to.latitude.toString().substring(0,9);
            coordinatesObject.toLat = Number(toLatString);
            var toLonString = to.longitude.toString().substring(0,9);
            coordinatesObject.toLon = Number(toLonString);

            return coordinatesObject;
        }
    }
}());