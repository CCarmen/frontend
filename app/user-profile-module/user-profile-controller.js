(function () {
    'use strict';

    angular.module('user-profile-module').controller('userProfileController', userProfileController);

    userProfileController.$inject = ['$rootScope', '$scope', '$state', 'userProfileService','flashService'];

    function userProfileController($rootScope, $scope, $state, userProfileService, flashService) {
        var vm = this;

        vm.submitForm = submitForm;
        vm.changePassword = changePassword;

        initUserProfileController();

        function initUserProfileController() {
            console.log('initUserProfileController');
            vm.user = {
                changingPassword: false
            };
        }

        function submitForm() {
            console.log('vm.user == ' + JSON.stringify(vm.user));

            if (vm.user.changingPassword) {
                var valid = checkConfirmPassword(vm.user.password, vm.user.confirmPassword);

                if(valid && valid != 'false') {
                    var newPassword = userProfileService.createNewPassword(vm.user);
                    userProfileService.changePassword(newPassword).then(
                        function (response) {
                            //success
                            flashService.Success("You successfully changed your password!");

                        }, function (error) {
                            //error
                            console.log('error login response ==' + JSON.stringify(error));
                        });
                } else {
                    flashService.Error("The confirmation password is different!")
                }
            } else {
                var userForUpdate = userProfileService.createUserForUpdate(vm.user);
                userProfileService.updateUserInfo(userForUpdate).then(
                    function (response) {
                        //success
                        flashService.Success("You successfully updated your information!");
                        $rootScope.session = {
                            userInfo: response.data
                        }
                    }, function (error) {
                        //error
                        console.log('error login response ==' + JSON.stringify(error));
                        if (error.status == '404') {
                            // $rootScope.authenticated = false;
                        }
                    });
            }
        }

        function checkConfirmPassword(password, confirmPassword) {
            if (angular.equals(password, confirmPassword)) {
                vm.editUserForm.$invalid = false;
                return true;

            } else {
                vm.editUserForm.$invalid = true;
                return false;
            }
        }

        function changePassword() {
            vm.user.changingPassword = true;
        }
    }
}());