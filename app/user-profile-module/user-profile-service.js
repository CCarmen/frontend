(function () {
    'use strict';

    angular.module('user-profile-module')
        .factory('userProfileService', userProfileService);

    userProfileService.$inject = ['$http', '$rootScope'];

    function userProfileService($http, $rootScope) {

        return {
            updateUserInfo: updateUserInfo,
            changePassword: changePassword,
            createNewPassword: createNewPassword,
            createUserForUpdate: createUserForUpdate
        };

        function updateUserInfo(user) {
            console.log('updateUser user == ' + JSON.stringify(user));
            return $http.post('http://localhost:8080/user/update', user);
        }

        function changePassword(passwordInfo) {
            console.log("passwordInfo password == "+JSON.stringify(passwordInfo));
            return $http.post('http://localhost:8080/user/password', passwordInfo);
        }

        function createNewPassword(user) {
            var newPasswordObject = {};

            newPasswordObject.userId = $rootScope.session.userInfo.id;
            newPasswordObject.oldPassword = user.oldPassword;
            newPasswordObject.newPassword = user.password;

            return newPasswordObject;
        }

        function createUserForUpdate(user) {
            var userForUpdate = {};

            userForUpdate.id = $rootScope.session.userInfo.id;
            userForUpdate.firstName = user.firstName;
            userForUpdate.lastName = user.lastName;
            userForUpdate.email = user.email;
            userForUpdate.username = user.username;
            userForUpdate.identityCard = user.identityCard;
            userForUpdate.creditCard = {};
            userForUpdate.creditCard.cardNumber = user.creditCard.cardNumber;
            userForUpdate.creditCard.cardName = user.creditCard.cardName;
            userForUpdate.creditCard.cvv = user.creditCard.cvv;


            return userForUpdate;
        }
    }
}());